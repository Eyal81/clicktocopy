// Initial References
const newTaskInput = document.querySelector("#new-task textarea");
const tasksDiv = document.querySelector("#tasks");
const tabList = document.querySelector("#tabList");
let updateNote = "";
let count;

// Function to get the count of items in local storage
const getItemCountInLocalStorage = () => {
    count = Object.keys(localStorage).length;
};

// Function to display tabs and tasks based on selected category
const displayTabsAndTasks = () => {
    const selectedCategory = document.querySelector("#category").value;
    clearTabsAndTasks();
    const categories = getUniqueCategories();
    createTabs(categories, selectedCategory);
    displayTasksByCategory(selectedCategory);
};

// Function to clear tabs and tasks
const clearTabsAndTasks = () => {
    tabList.innerHTML = "";
    tasksDiv.innerHTML = "";
};

// Function to fetch unique categories from local storage
const getUniqueCategories = () => {
    const categorySelect = document.querySelector("#category");

    const categories = new Set();
    for (const key in localStorage) {
        if (localStorage.hasOwnProperty(key)) {
            const category = key.split("_")[2];

            if (!categorySelect.querySelector(`option[value="${category}"]`)) {
                const option = document.createElement("option");
                option.value = category;
                option.textContent = category;
                categorySelect.appendChild(option);
            }
            categories.add(category);
        }
    }
    return categories;
};

// Function to create tabs for each category
const createTabs = (categories, selectedCategory) => {
    categories.forEach((category) => {
        const tab = createTabElement(category, selectedCategory);
        tabList.appendChild(tab);
    });
};

// Function to create a single tab element
const createTabElement = (category, selectedCategory) => {
    const tab = document.createElement("li");
    tab.textContent = category;
    tab.classList.add("tab");
    if (category === selectedCategory) {
        tab.classList.add("active");
    }
    tab.addEventListener("click", () => {
        activateTab(tab);
        displayTasksByCategory(category);
    });
    return tab;
};

// Function to activate the selected tab
const activateTab = (clickedTab) => {
    document.querySelectorAll(".tab").forEach((tab) => {
        tab.classList.remove("active");
    });
    clickedTab.classList.add("active");
};

// Function to display tasks based on the selected category
const displayTasksByCategory = (category) => {
    clearTasks();
    for (const key in localStorage) {
        if (localStorage.hasOwnProperty(key)) {
            const taskCategory = key.split("_")[2];
            if (taskCategory === category) {
                displayTask(key);
            }
        }
    }
};

// Function to clear the tasks container
const clearTasks = () => {
    tasksDiv.innerHTML = "";
};

// Function to create a task element
const createTaskElement = (taskName) => {
    const taskInnerDiv = document.createElement("div");
    taskInnerDiv.classList.add("task");
    taskInnerDiv.innerHTML = `<span id="taskname" dir="rtl" style="white-space:pre-wrap">${taskName}</span>`;
    return taskInnerDiv;
};

// Function to create an edit button for a task
const createEditButton = () => {
    const editButton = document.createElement("button");
    editButton.classList.add("edit");
    editButton.innerHTML = `<i class="fa-solid fa-pen-to-square"></i>`;
    return editButton;
};

// Function to create a delete button for a task
const createDeleteButton = () => {
    const deleteButton = document.createElement("button");
    deleteButton.classList.add("delete");
    deleteButton.innerHTML = `<i class="fa-solid fa-trash"></i>`;
    return deleteButton;
};

// Function to attach event listeners to edit and delete buttons
const attachEventListeners = (taskElement, key) => {
    const editButton = taskElement.querySelector(".edit");
    const deleteButton = taskElement.querySelector(".delete");

    // Edit Tasks
    editButton.addEventListener("click", (e) => {
        handleEditClick(key);
    });

    // Delete Tasks
    deleteButton.addEventListener("click", (e) => {
        handleDeleteClick(key, taskElement);
    });
};

// Function to handle edit button click
const handleEditClick = (key) => {
    const taskName = key.split("_")[1];
    navigator.clipboard.writeText(taskName).then(() => {
        console.log('Content copied to clipboard');
        new Toast('הטקסט הועתק בהצלחה', ToastType.Success, 5000);
    }, () => {
        console.error('Failed to copy');
        new Toast('ארע שגיאה בתהליך העתקה', ToastType.Error, 5000);
    });
};

// Function to handle delete button click
const handleDeleteClick = (key, taskElement) => {
    removeTask(key);
    taskElement.remove();
    count -= 1;
    new Toast('הטקסט נמחק בהצלחה', ToastType.Success, 5000);
};

// Function to display a task
const displayTask = (key) => {
    const taskName = key.split("_")[1];

    const taskElement = createTaskElement(taskName);
    const editButton = createEditButton();
    const deleteButton = createDeleteButton();

    taskElement.appendChild(editButton);
    taskElement.appendChild(deleteButton);
    tasksDiv.appendChild(taskElement);

    attachEventListeners(taskElement, key);
};


// Function to remove a task from local storage
const removeTask = (taskValue) => {
    localStorage.removeItem(taskValue);
    displayTabsAndTasks();
};

// Function to add tasks to local storage
const updateStorage = (index, taskValue, selectedCategory) => {
    localStorage.setItem(`${index}_${taskValue}_${selectedCategory}`, false);
    displayTabsAndTasks();
};

// Function to handle the "Add" button click
const handleAddButtonClick = () => {
    const customCategoryInput = document.querySelector("#customCategory");
    const customCategory = customCategoryInput.value.trim();
    let selectedCategory = "";
    if (customCategory === "") {
        selectedCategory = document.querySelector("#category").value;
    }
    else {
        selectedCategory = customCategory;
        addCustomCategory(customCategory);
    }

    const taskValue = newTaskInput.value.trim();
    if (taskValue.length === 0) {
        alert("Please Enter A Task");
    } else {
        if (updateNote === "") {
            updateStorage(count, taskValue, selectedCategory);
        }
        count += 1;
        newTaskInput.value = "";
    }
};

// Function to add a custom category
const addCustomCategory = (customCategory) => {
    const customCategoryInput = document.querySelector("#customCategory");
    const categorySelect = document.querySelector("#category");
    const taskValue = newTaskInput.value.trim();

    // Add the custom category as an option
    const option = document.createElement("option");
    option.value = customCategory;
    option.textContent = customCategory;

    // Select the custom category
    categorySelect.value = customCategory;

    // Clear the custom category input
    customCategoryInput.value = "";

    // Update the tasks list
    displayTabsAndTasks();
};


// Function to create and display toast notifications
class Toast {
    constructor(message, color, time) {
        this.message = message;
        this.color = color;
        this.time = time;
        this.element = null;
        const element = document.createElement('div');
        element.className = "toast-notification";
        this.element = element;
        const countElements = document.getElementsByClassName("toast-notification");

        element.style.opacity = 0.8;

        element.style.marginBottom = (countElements.length * 55) + "px";

        element.style.backgroundColor = this.color;

        const messageElement = document.createElement("div");
        messageElement.className = "message-container";
        messageElement.textContent = this.message;

        element.appendChild(messageElement);

        const close = document.createElement("div");
        close.className = "close-notification";

        const icon = document.createElement("i");
        icon.className = "lni lni-close";

        close.appendChild(icon);
        element.append(close);

        document.body.appendChild(element);

        setTimeout(() => {
            element.remove();
        }, this.time);

        close.addEventListener("click", () => {
            element.remove();
        });
    }
}
const ToastType = {
    Error: "#eb3b5a",
    Warning: "#fdcb6e",
    Success: "#00b894",
};

// Initial display of tabs and tasks
window.onload = () => {
    getItemCountInLocalStorage();
    displayTabsAndTasks();
};

// Add event listener to the "Add" button
document.querySelector("#push").addEventListener("click", handleAddButtonClick);
